/*
 * oauth2-oidc-sdk
 *
 * Copyright 2012-2025, Connect2id Ltd and contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed
 * under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */

package com.nimbusds.oauth2.sdk.ciba;

import com.nimbusds.oauth2.sdk.id.Identifier;
import net.jcip.annotations.Immutable;


/**
 * Login hint token.
 *
 * <p>Related specifications:
 *
 * <ul>
 *      <li>OpenID Connect CIBA Flow - Core 1.0.
 * </ul>
 */
@Immutable
public class LoginHintToken extends Identifier {


        /**
         * Creates a new login hint token with the specified value.
         *
         * @param value The value. Must not be {@code null} or empty string.
         */
        public LoginHintToken(final String value) {
                super(value);
        }


        /**
         * Creates a new login hint token with a randomly generated 256-bit
         * (32-byte) value, Base64URL-encoded.
         */
        public LoginHintToken() {
                super();
        }


        @Override
        public boolean equals(final Object object) {

                return object instanceof LoginHintToken &&
                        this.toString().equals(object.toString());
        }
}

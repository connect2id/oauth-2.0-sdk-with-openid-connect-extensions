/*
 * oauth2-oidc-sdk
 *
 * Copyright 2012-2024, Connect2id Ltd and contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed
 * under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */

package com.nimbusds.oauth2.sdk.ciba;

import com.nimbusds.jose.util.Base64URL;
import com.nimbusds.jose.util.ByteUtils;
import org.junit.Test;

import static org.junit.Assert.*;

public class LoginHintTokenTest {


        @Test
        public void valueConstructor() {

                String value = "475b3872-8287-4872-9b94-eb4ac26d3ee9";
                LoginHintToken token = new LoginHintToken(value);
                assertEquals(value, token.getValue());
                assertEquals(value, token.toString());
                assertEquals("\"" + value + "\"", token.toJSONString());

                assertEquals(token, new LoginHintToken(value));
                assertEquals(token.hashCode(), new LoginHintToken(value).hashCode());
        }


        @Test
        public void randomConstructor() {

                LoginHintToken token = new LoginHintToken();
                Base64URL base64URL = new Base64URL(token.getValue());
                assertEquals(256, ByteUtils.bitLength(base64URL.decode()));

                assertEquals(token, new LoginHintToken(token.getValue()));
                assertEquals(token.hashCode(), new LoginHintToken(token.getValue()).hashCode());
        }
}